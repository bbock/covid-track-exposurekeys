import datetime
import io
import logging
import urllib.request
import zipfile

import boto3

import export_pb2

logging.basicConfig(level=logging.INFO)


def get_exposurekeys(hour=None):
    """
    Get exposure keys from corona-warn-app server

    This function downloads the data for yesterday (it is only updated after
    24h grace perion when exposure keys are rotated) and parses it.

    It needs to unpack the zip file and parse the protobuf-encoded data.

    Not implemented: signature check
    """
    yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
    export_url = (
        "https://svc90.main.px.t-online.de/version/v1/diagnosis-keys/country/DE/date/"
        + yesterday.strftime("%F")
    )
    if hour:
        export_url = export_url + "/hour/" + str(hour)
    cwa_zip_req = urllib.request.urlopen(export_url)
    cwa_zip_file = zipfile.ZipFile(io.BytesIO(cwa_zip_req.read()))

    data = cwa_zip_file.open("export.bin")
    data.seek(16)  # skip header

    cwa_export = export_pb2.TemporaryExposureKeyExport()
    cwa_export.ParseFromString(data.read())
    return cwa_export


def calculate_riskdensity(exposurekeys):
    """ Calculates the density (histogram) of the risk levels in the exposure keys """
    risk_levels = [0] * 8
    for k in exposurekeys.keys:  # pylint: disable=no-member
        risk_levels[k.transmission_risk_level - 1] += 1
    return risk_levels


def cron(event, context):  # pylint: disable=unused-argument
    """ AWS Lambda is calling this handler as a cronjob """
    cloudwatch = boto3.client("cloudwatch")
    exposurekeys = get_exposurekeys()
    exposurekeys_riskdensity = calculate_riskdensity(exposurekeys)

    metricdata = []
    for risk in range(0, 8):
        metricdata.append(
            {
                # risk levels start at 1, array index at 0
                "MetricName": "risk" + str(risk + 1),
                "Dimensions": [{"Name": "COUNTRY", "Value": "DE"}],
                "Unit": "None",
                "Value": exposurekeys_riskdensity[risk],
            }
        )

    response = cloudwatch.put_metric_data(MetricData=metricdata, Namespace="covid")
    logging.info(response)


if __name__ == "__main__":
    print(calculate_riskdensity(get_exposurekeys()))
