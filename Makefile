.PHONY: env clean all

all: env handler.py
	npx serverless package

env:
	virtualenv -p python3.7 env
	./env/bin/pip3 install -r requirements.txt
	npm install

clean:
	rm -rf *_pb2.py .serverless __pycache__ node_modules env package-lock.json

export_pb2.py: export.proto
	protoc --python_out=. export.proto

handler.py: export_pb2.py
