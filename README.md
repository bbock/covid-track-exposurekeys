Function to collect exposure key data from the corona-warn-app (CWA) server and save statistics into AWS CloudWatch.

Be aware that in order to assure privacy, some of the keys are fake keys. So, this application can only give indicative numbers. The reported numbers of keys must not be regarded as exact values.

This application is mainly inteded as a showcase for AWS (Lambda, Cloudwatch) and the Serverless Framework.


## Installing prerequisites for building

Requirements: You need **python3.7** and **node.js** (Release 6.x or higher, including **npm**) and the **protobuf compiler**.

For Ubuntu, you can install all of these with

```bash
apt install -y nodejs npm python3.7 python3-pip protobuf-compiler
make env
```

## Build and deploy

To build the app locally, call `make` to generate the classes from the protobuf definition.  
You can run it via the serverless framework if you have configured credentials in `~/.aws/credentials`.

```bash
source env/bin/activate  # activate python virtualenv
./node_modules/.bin/serverless invoke local -f cron
```

Deploy via `npx serverless deploy`.

The GitLab CI job is configured to do all of that automatically.


## External Documentation

- [OpenAPI endpoints](https://github.com/corona-warn-app/cwa-server/blob/master/services/distribution/api_v1.json)
- [Protobuf file format definition](https://developers.google.com/android/exposure-notifications/exposure-key-file-format)
- [Serverless Framework](https://www.serverless.com/framework/docs/)


## Results

View stats [here](https://eu-west-1.console.aws.amazon.com/cloudwatch/home?region=eu-west-1#metricsV2:graph=~();namespace=~'covid).
